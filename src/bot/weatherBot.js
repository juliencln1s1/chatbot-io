import Bot from './bot';

class WeatherBot extends Bot {
  constructor(name, avatar) {
    super(name, avatar);
    this.addAction('temperature', this.getWeather);
    this.addAction('prevision', this.getForecast);
    this.addAction('humidite', this.getHumidity);
  }

  async getWeather() {
    try {
      const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=48.1173&longitude=-1.6778&hourly=temperature_2m');
      const data = await response.json();
      return `Température actuelle à Rennes: ${data.hourly.temperature_2m[0]}°C`;
    } catch (error) {
      return 'Failed to fetch weather data';
    }
  }

  async getForecast() {
    try {
      const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=48.1173&longitude=-1.6778&daily=temperature_2m_max,temperature_2m_min');
      const data = await response.json();
      return `Les prévisions à Rennes sont: Temp Max: ${data.daily.temperature_2m_max[0]}°C, Temp Min: ${data.daily.temperature_2m_min[0]}°C`;
    } catch (error) {
      return 'Failed to fetch forecast data';
    }
  }

  async getHumidity() {
    try {
      const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=48.1173&longitude=-1.6778&hourly=relative_humidity_2m');
      const data = await response.json();
      return `Humidité à Rennes: ${data.hourly.relative_humidity_2m[0]}%`;
    } catch (error) {
      return 'Failed to fetch humidity data';
    }
  }
}

export default WeatherBot;
