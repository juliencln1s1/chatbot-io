import './index.scss';
import Bot from './bot/bot';
import WeatherBot from './bot/weatherBot';
import askGPT from './services/api';

const bot1 = new Bot('Sansa Stark', 'avatar1.png');
bot1.addAction('greet', () => 'Hello! How can I assist you today?');
bot1.addAction('time', () => `Current time is: ${new Date().toLocaleTimeString()}`);
bot1.addAction('date', () => `Today's date is: ${new Date().toLocaleDateString()}`);
bot1.addAction('broadcast', () => 'Broadcasting to all bots!');
bot1.addAction('ask', async () => askGPT('What is the capital of France?'));

const bot2 = new Bot('Tyrion Lannister', 'avatar2.png');
bot2.addAction('quote', () => 'I drink and I know things.');
bot2.addAction('advice', () => 'Never forget what you are. The rest of the world will not.');
bot2.addAction('joke', () => 'I am the god of tits and wine.');
bot2.addAction('ask', async () => askGPT('Tell me a joke'));

const weatherBot = new WeatherBot('WeatherBot', 'avatar3.png');

const bots = [bot1, bot2, weatherBot];

class Template {
  constructor() {
    this.el = document.querySelector('body');
    this.messageInput = null;
    this.selectedBot = null;
    this.loadHistory();
  }

  render() {
    return `
    <div class="container py-5 px-4">
      <div class="row rounded-lg overflow-hidden shadow box">
        <div class="col-5 px-0">
          <div class="bg-white h-100">
            <div class="bg-gray px-4 py-2 bg-light">
              <p class="h5 mb-0 py-1">Chatbots IO</p>
            </div>
            <div class="messages-box">
              <div class="list-group rounded-0">
                ${bots.map((bot, index) => `
                  <a class="list-group-item list-group-item-action rounded-0 bot-list-item" data-index="${index}">
                    <div class="d-flex align-items-center">
                      <img class="avatar" src="https://static-00.iconduck.com/assets.00/user-avatar-robot-icon-2048x2048-ehqvhi4d.png" width="50">
                      <div class="mx-2 flex-grow-1">
                        <div class="d-flex align-items-center justify-content-between mb-1">
                          <h6 class="mb-0">${bot.name}</h6>
                        </div>
                      </div>
                    </div>
                  </a>`).join('')}
              </div>
            </div>
          </div>
        </div>
        <div class="col-7 px-0">
          <div class="px-4 py-5 chat-box bg-white" id="chat-box">
          </div>
          <div class="input-group p-2">
            <input type="text" class="form-control" placeholder="Taper un message" aria-label="Taper un message" aria-describedby="button-addon2" id="message-input">
            <button class="btn btn-outline-primary" type="button" id="button-addon2">Envoyer</button>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  run() {
    this.el.innerHTML = this.render();
    this.messageInput = document.getElementById('message-input');
    const sendButton = document.getElementById('button-addon2');
    sendButton.addEventListener('click', () => this.sendMessage());

    this.messageInput.addEventListener('keydown', (event) => {
      if (event.key === 'Enter') {
        this.sendMessage();
      }
    });

    const botListItems = document.querySelectorAll('.bot-list-item');
    botListItems.forEach((item) => {
      item.addEventListener('click', (event) => this.selectBot(event));
    });
  }

  selectBot(event) {
    const index = event.currentTarget.getAttribute('data-index');
    this.selectedBot = bots[index];
    document.querySelectorAll('.bot-list-item').forEach((item) => item.classList.remove('active'));
    event.currentTarget.classList.add('active');
    this.renderChatBox();
    this.appendMessage('System', `Vous discutez désormais avec ${this.selectedBot.name}`, false, true);
    this.saveHistory(); // Sauvegarder l'historique après la sélection d'un bot
  }

  saveHistory() {
    const history = bots.map((bot) => ({
      name: bot.name,
      history: bot.history
    }));
    localStorage.setItem('chatbotHistory', JSON.stringify(history));
  }

  loadHistory() {
    const history = JSON.parse(localStorage.getItem('chatbotHistory'));
    if (history) {
      history.forEach((savedBot) => {
        const bot = bots.find((b) => b.name === savedBot.name);
        if (bot) {
          bot.history = savedBot.history;
        }
      });
    }
  }

  async sendMessage() {
    const message = this.messageInput.value;
    if (message.trim() === '' || !this.selectedBot) return;
    const currentTime = new Date();
    this.appendMessage('You', message, true, false, currentTime);
    this.selectedBot.history.push({ sender: 'You', message, timestamp: currentTime });

    const commands = this.selectedBot.help();
    const responses = await Promise.all(
      commands.filter((command) => message.includes(command))
        .map((command) => this.selectedBot.performAction(command))
    );

    responses.forEach((response) => {
      const responseTime = new Date();
      this.appendMessage(this.selectedBot.name, response, false, false, responseTime);
      this.selectedBot.history.push({
        sender: this.selectedBot.name,
        message: response,
        timestamp: responseTime
      });
    });

    this.messageInput.value = '';
    this.saveHistory(); // Sauvegarder l'historique après chaque message
  }

  renderChatBox() {
    const chatBox = document.getElementById('chat-box');
    chatBox.innerHTML = ''; // Vider la zone de chat actuelle
    this.selectedBot.history.forEach((entry) => {
      this.appendMessage(entry.sender, entry.message, entry.sender === 'You', entry.sender === 'System', new Date(entry.timestamp));
    });
  }

  appendMessage(sender, message, isUser, isSystem, timestamp = new Date()) {
    const chatBox = document.getElementById('chat-box');
    const formattedTime = `${timestamp.toLocaleDateString()} ${timestamp.toLocaleTimeString()}`;
    let messageElement;
    if (isSystem) {
      messageElement = `
        <div class="w-100 mb-3 text-x-small">
          <div class="text-center text-muted">${message}</div>
        </div>`;
    } else {
      messageElement = `
      <div class="media w-50 ${isUser ? 'ms-auto mb-3' : 'mb-3'}">
        ${!isUser ? '<img src="https://static-00.iconduck.com/assets.00/user-avatar-robot-icon-2048x2048-ehqvhi4d.png" alt="user" width="50" class="rounded-circle">' : ''}
        <div class="media-body ${isUser ? '' : 'ml-3'}">
          <div class="bg-${isUser ? 'primary' : 'light'} rounded py-2 px-3 mb-2">
            <p class="text-small mb-0 ${isUser ? 'text-white' : 'text-muted'}">${message}</p>
          </div>
          <p class="small text-muted">${formattedTime}</p>
        </div>
      </div>`;
    }
    chatBox.insertAdjacentHTML('beforeend', messageElement);
    chatBox.scrollTop = chatBox.scrollHeight;
  }
}

const template = new Template();
template.run();
